@echo off
setlocal EnableDelayedExpansion EnableExtensions


for /R "dsrc" %%G in (*.tdf) do (
    set "ifile=%%G"
	
	    "tools/TemplateDefinitionCompiler.exe" -compile !ifile!
)

goto :eof

:dir_from_path <resultVar> <pathVar>
(
    set "%~1=%~d2%~p2"
 
)
:eof
endlocal